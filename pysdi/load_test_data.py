# -*- coding: utf-8 -*-
"""
Load test data.
Created on Tue Mar 31 12:34:53 2020
@author: Roberto A. Real-Rangel (rrealr@iingen.unam.mx)
"""
from pathlib2 import Path as _Path
import xarray as _xr


def load_test_data():
    return(_xr.open_dataset(filename_or_obj=str('test_input_data.nc4')))
